package com.binar.service;

import java.util.Scanner;

public abstract class Menuku {
    private final Scanner input = new Scanner(System.in);

    public byte promptInput(){
        System.out.println("-----------------------------------------------------------");
        System.out.print("Masukkan pilihan : ");
        return input.nextByte();
    }
    public abstract void tampilMenu();

}
