package com.binar.service;

public class MainMenu extends Menuku {
    public String csvPath = "src/main/resources/data_sekolah.csv";
    public String saveMeanMedModPath = "src/main/resources/MeanMedMod_Sekolah.txt";
    public String saveGroupPath = "src/main/resources/Pengelompokkan_Sekolah.txt";
    public String saveKeduanyaPath = "src/main/resources/File_gabungan.txt";

    @Override
    public void tampilMenu(){
        MeanMedMod file = new MeanMedMod();
        MenuSelesai selesai = new MenuSelesai();
        System.out.println("-----------------------------------------------------------");
        System.out.println("              Aplikasi Pengolah Nilai Siswa                ");
        System.out.println("-----------------------------------------------------------");
        System.out.println("File akan dibaca di "+csvPath);
        System.out.println("-----------------------------------------------------------" +
                "\nSilahkan masukkan pilhan Anda : " +
                "\n1. Generate File txt Pengelompokkan Nilai" +
                "\n2. Generate File txt Mean-Median-Modus" +
                "\n3. Generate File txt Kedua File" +
                "\n0. Exit");
        switch (promptInput()){
            case 0 :
                System.out.println("Program sedang ditutup");
                System.exit(0);
                break;
            case 1 :
                file.writeGroup(saveGroupPath);
                System.out.println("File telah tergenerate di "+saveGroupPath);
                selesai.tampilMenu();
                break;
            case 2 :
                file.writemeanmedmod(saveMeanMedModPath);
                System.out.println("File telah tergenerate di "+saveMeanMedModPath);
                selesai.tampilMenu();
                break;
            case 3 :
                file.writemix(saveKeduanyaPath);
                System.out.println("File telah tergenerate di "+saveKeduanyaPath);
                selesai.tampilMenu();
                break;
            default:
                System.out.println("Masukkan pilihan yang benar");
                this.tampilMenu();
                break;
        }

    }
}
