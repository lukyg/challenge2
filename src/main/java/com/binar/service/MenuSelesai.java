package com.binar.service;

public class MenuSelesai extends Menuku{

    @Override
    public void tampilMenu(){
        MainMenu main = new MainMenu();
        System.out.println("File berhasil diproses!" +
                "\n1. Kembali ke Menu Utama" +
                "\n0. Exit");
        switch (promptInput()){
            case 0 :
                System.out.println("Program sedang ditutup");
                System.exit(0);
                break;
            case 1 :
                main.tampilMenu();
                break;
            default:
                System.out.println("Pilihan menu tidak tersedia");
                this.tampilMenu();
                break;
        }
    }
}
