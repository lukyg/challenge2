package com.binar.service;

import java.io.*;
import java.util.*;

public class MeanMedMod<var> {
    public <String>List<Integer> read(String path) {
        try {
            File file = new File(java.lang.String.valueOf(path));
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            java.lang.String line = " ";
            java.lang.String[] tempArr;

            List<Integer> listInt = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        java.lang.String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }

                }
            }
            reader.close();
            return listInt;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeGroup(String savePlace) {

        MainMenu menu = new MainMenu();

        try {
            File file = new File(savePlace);
            if (file.createNewFile()) {
                System.out.println("File akan disimpan di " + savePlace);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai: ");
            bwr.newLine();
            bwr.newLine();

            // Menghitung mean
            bwr.write("Mean   : " + String.format("%.2f", mean(read(menu.csvPath))));
            bwr.newLine();

            // Menghitung median
            bwr.write("Median : " + median(read(menu.csvPath)));
            bwr.newLine();

            // Menghitung modus
            bwr.write("Modus  : " + mode(read(menu.csvPath)));
            bwr.newLine();

            bwr.flush();
            bwr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writemeanmedmod(String saveMod) {

        MainMenu menu = new MainMenu();

        try {
            File file = new File(saveMod);
            if (file.createNewFile()) {
                System.out.println("File akan disimpan di " + saveMod);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            Map<Integer, Integer> hMap = freq(read(menu.csvPath));

            bwr.write("Berikut Hasil Pengolahan Nilai:");
            bwr.write("\n\n");
            bwr.write("| Nilai " + "|" + " Frekuensi "  + "|\n");
            bwr.write("|\t5\t"    + "|\t  " + hMap.get(5)  + " \t|\n");
            bwr.write("|\t6\t"    + "|\t  " + hMap.get(6)  + " \t|\n");
            bwr.write("|\t7\t"    + "|\t  " + hMap.get(7)  + " \t|\n");
            bwr.write("|\t8\t"    + "|\t  " + hMap.get(8)  + " \t|\n");
            bwr.write("|\t9\t"    + "|\t  " + hMap.get(9)  + " \t|\n");
            bwr.write("|\t10\t"   + "|\t  " + hMap.get(10) + " \t|\n");


            bwr.flush();
            bwr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writemix (String saveMix){
        MainMenu menu = new MainMenu();
        try {
            File file = new File(saveMix);
            if (file.createNewFile()){
                System.out.println("File akan disimpan di " + saveMix);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai:");
            bwr.write("\n\n");

            // Menghitung mean
            bwr.write("Mean   : " + String.format("%.2f", mean(read(menu.csvPath))));
            bwr.newLine();

            // MEDIAN
            bwr.write("Median : " + median(read(menu.csvPath)));
            bwr.newLine();

            // MODUS
            bwr.write("Modus  : " + mode(read(menu.csvPath)));
            bwr.newLine();

            Map<Integer, Integer> hMap = freq(read(menu.csvPath));

            bwr.newLine();
            bwr.write("Berikut Hasil Pengolahan Nilai: ");
            bwr.write("\n\n");
            bwr.write("| Nilai " + "|" + " Frekuensi "  + "|\n");
            bwr.write("|\t5\t"    + "|\t  " + hMap.get(5)  + " \t|\n");
            bwr.write("|\t6\t"    + "|\t  " + hMap.get(6)  + " \t|\n");
            bwr.write("|\t7\t"    + "|\t  " + hMap.get(7)  + " \t|\n");
            bwr.write("|\t8\t"    + "|\t  " + hMap.get(8)  + " \t|\n");
            bwr.write("|\t9\t"    + "|\t  " + hMap.get(9)  + " \t|\n");
            bwr.write("|\t10\t"   + "|\t  " + hMap.get(10) + " \t|\n");

            bwr.flush();
            bwr.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // RUMUS
    private double mean(List<Integer> list) {
        return list.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

    private double median(List<Integer> numArray) {
        Arrays.sort(new List[]{numArray});
        double median;
        if (numArray.size() % 2 == 0)
            median = ((double) numArray.get(numArray.size() / 2) + (double) numArray.get(numArray.size() / 2 - 1)) / 2;
        else
            median = (double) numArray.get(numArray.size() / 2);
        return median;
    }

    private int mode(List<Integer> array) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : array) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }

    // Mapping Modus
    public Map<Integer, Integer> freq(List<Integer> array) {
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }
}
